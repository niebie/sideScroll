#ifndef _TICK_FUNCTIONS_H_
#define _TICK_FUNCTIONS_H_

void updateTick();
void drawTick();
void finalizeTick();

#endif
