/*
  This is a file that has all of the common things to the program.
 */

#ifndef _COMMON_H_
#define _COMMON_H_

// Define DEBUG here. Used throughout to conditionally print, etc.
// #define DEBUG

// Call out standard includes.
#include <stdio.h>
#include <stdlib.h>

// This is the global return value definition, of which
// glo_res should be used.
enum returnValues
  {
    ret_norm = 0,
    ret_fail = 1
  };

#endif
