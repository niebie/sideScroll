#ifndef _SDL_MANAGER_H_
#define _SDL_MANAGER_H_

bool initializeSDL();
void loadTestImage();
void displaySurfaceToWindow();
void finalizeSDL();

#endif
