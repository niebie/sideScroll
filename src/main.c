/*
  ENTRY POINT TO SIDE SCROLLER ENGINE
 */

#include <stdbool.h>
#include <common.h>
#include <SDL2/SDL.h>

#include <sdl-manager.h>
#include <sdl-event-manager.h>
#include <tick-funcs.h>

#define MAIN_CONFIG "/home/niebieskitrociny/git/c/sideScroll/configs/main.lua"
#include <custom_config.h>

int main(int argc, char** argv)
{
  int lResult = 0;
  unsigned int lTicks = 0;
  unsigned int lOldTicks = 0;
  unsigned int lTickDiff = 0;

  //Internal library initializer.
  //bool lSuccess = initializeSDL( );

  // TODO > Should i use a manifest file?


  /*
  // Start-up the main loop.
  for( / *No loop initializer ;
  / *No loop condition   ;
  lOldTicks = lTicks,
  lTicks = SDL_GetTicks( ),
  lTickDiff = lTicks - lOldTicks )
  {
  // Check the event stack, process all events.
  processEventQueue( );

  // Update all objects state data based on incoming events,
  // and the global time step.
  updateTick( lTickDiff );

  // Draw all current positions onto the second buffer.
  drawTick( );

  // Swap the buffers, and perform any tick-finalizers.
  finalizeTick( );

  // Before we go, update the ticks with the current time.
  lTicks = SDL_GetTicks( );

  // Wait a milli-second, so that the processor does not peg.
  SDL_Delay( 1 / *[ms] );
  }
  */

  if( load_primary_config( MAIN_CONFIG ) )
    {
      printf( "LUA CONFIG FILE LOADING FAILED\n" );
    }
  else
    {
      printf( "Loaded the config file successfully.\n" );
    }

  return EXIT_SUCCESS;
}
