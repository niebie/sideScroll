/*
  This file defines different utilities based on SDL_Windows.
 */

#define TITLE_LEN 1024



void setTitle(char* aTitleString)
{
  // We use an internal static variable (quite large) to represent
  // the contents of a title string.
  static char sTitle[TITLE_LEN];

  // ALL STRINGS IN THE SYSTEM ARE:
  // 1) c-strings
  // AND EITHER
  // a) statically allocated literals
  // OR
  // b) dynamic, kilo-byte sized character arrays.
  // Therefor, we use strnlen and let any other string
  // passed in remain un-defined in behavior.

  strncpy( sTitle, aTitleString, TITLE_LEN );

  // TODO > Double buffer? This seems like on _very_
  //      > slow systems that the copy operation
  //      > might be expensive, and visible.

  // Verify that the current pointer is to the static
  // title string.
  SDL_SetWindowTitle
}

