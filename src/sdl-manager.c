/*
  This file serves to define functions that manage the SDL context for us.
 */
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <stdbool.h>

#include <sdl-manager.h>

//These are all intentionally file-scoped to only allow access to
//them from functions in this file.
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

SDL_Window *fWindow = NULL;
SDL_Surface *fScreenSurface = NULL;
SDL_Surface *fImageContainer = NULL;

bool fInitialized = false;

/********************************************************************
 * This function will perform the standard steps to initialize
 * the window/screen/internals of SDL.
 ********************************************************************/
bool initializeSDL()
{
  // No matter what, call our finalize routine.
  atexit( finalizeSDL );

  if( fInitialized == true )
    {
      printf( "SDL has already been initialized!!\n" );
      return fInitialized;
    }

  //If it is not already set to true, we set it to true and then run the
  //initialization routine. If something fails it will be set to false.
  fInitialized = true;

  //0 is success.
  if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
      printf( "SDL failed its initialization process.\n" );
      // SDL_GetError() uses a static char[], so we do NOT free the result
      // nor does the pointer go away when another function is called.
      printf( "SDL_Error: %s\n", SDL_GetError() );
      fInitialized = false;
      return fInitialized;
    }

  //Now that the internals of SDL are started up, we can get the basics of
  //what we are using initialized.
  fWindow = SDL_CreateWindow( "TITLE TESTING 123...", /* Window Title */
			      SDL_WINDOWPOS_UNDEFINED, /* X position */
			      SDL_WINDOWPOS_UNDEFINED, /* Y position */
			      SCREEN_WIDTH, /* Window Width */
			      SCREEN_HEIGHT, /* Window Height */
			      SDL_WINDOW_SHOWN); /* SDL Flags */

  if( fWindow == NULL )
    {
      printf( "Problems when creating an SDL WINDOW!" );
      printf( "SDL_Error: %s\n", SDL_GetError() );
      fInitialized = false;
      return fInitialized;
    }

  fScreenSurface = SDL_GetWindowSurface( fWindow );

  if( fScreenSurface == NULL )
    {
      printf( "Problems when getting the SDL Screen associated with the window!" );
      printf( "SDL_Error: %s\n", SDL_GetError() );
      fInitialized = false;
      return fInitialized;
    }

  //Clear the screen.
  if( SDL_FillRect( fScreenSurface, /* Destination Surface */
		    NULL , /* SDL_Rectangle (Null to fill the entire surface */
		    /* Embedded function to map the color we want ( White ) */
		    SDL_MapRGB( fScreenSurface->format, /* Description of the pixel format. */
				0xFF,                   /* Red   Component (0-255) */
				0xFF,                   /* Green Component (0-255) */
				0xFF ) ) < 0 )          /* Blue  Component (0-255) */
    {
      printf( "Problems when clearing the window's screen!" );
      printf( "SDL_Error: %s\n", SDL_GetError() );
      fInitialized = false;
      return fInitialized;
    }

  if( SDL_UpdateWindowSurface( fWindow ) < 0 )
    {
      printf( "Problems when clearing the window's screen!" );
      printf( "SDL_Error: %s\n", SDL_GetError() );
      fInitialized = false;
      return fInitialized;
    }

  return fInitialized;
}

bool failIfNotInit()
{
  if( fInitialized == false )
    {
      printf( "Called a function without successfully initializing SDL!" );
      return true;
    }
  return false;
}

/***************************************************************************
 * This function will load an image.
 ***************************************************************************/
void loadTestImage()
{
  if( failIfNotInit() == true )
    return;

  fImageContainer = SDL_LoadBMP( "../generalAssets/export/bmp/60_80.bmp" );
  if( fImageContainer == NULL)
    {
      printf( "Problems loading the requested file!\n" );
      printf( "SDL_Error: %s\n", SDL_GetError() );
      return;
    }
}

void displaySurfaceToWindow()
{
  if( failIfNotInit() == true )
    return;

  SDL_BlitSurface( fImageContainer, /* Source Surface to pull from */
		   NULL , /* Rectangle to use from the source surface. NULL copies whole surface */
		   fScreenSurface, /* Destination surface to draw to */
		   NULL ); /* Destination rectange to draw over. NOTE > Only X,Y coords matter. NULL to copy to 0,0 */

  SDL_UpdateWindowSurface( fWindow );
}

/***************************************************************************
 * This function will clean up the mess of SDL when we are done.
 ***************************************************************************/
void finalizeSDL()
{
  // Null is safe to pass to this.
  SDL_FreeSurface( fImageContainer );
  fImageContainer = NULL;

  // TODO > Check the SDL Error number?
  SDL_DestroyWindow( fWindow );
  fWindow = NULL;
  fScreenSurface = NULL;

  SDL_Quit( );
}
