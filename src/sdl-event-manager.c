/*
  This file manages the event queue.
 */

#include <sdl-event-manager.h>

// NOTE > Use pointers, because we don't want to put it on the stack.
void handleKeyPress( SDL_EventType *aEvent )
{
  // Get the key pressed and its state data ( up/down, mods )
}

void processEventQueue( )
{
  SDL_Event lEvent;

  // SDL_PollEvent returns a standard boolean.
  while( SDL_PollEvent( &lEvent ) )
    {
      // At this point, we found an event and need to propagate
      // it to all objects that care about it.

      // TODO > Process more than quiting.
      if( lEvent.type == SDL_QUIT )
        {
          exit( ret_norm );
        }
      else if( lEvent.type == SDL_KEYDOWN ||
               lEvent.type == SDL_KEYUP )
        {
          handleKeyPress( &lEvent );
        }
      else
        {
#ifdef DEBUG
          printf( "Found an unhandled event! Neat!\n" );
          printf( "Type :%d\n", lEvent.type );
#endif
        }
    }
}
