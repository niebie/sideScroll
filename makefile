#=======================================================================
# This is the highest level makefile for the side scroll game.         =
#=======================================================================

#=======================================================================
# General variables                                                    =
#=======================================================================
#C Compiler
CC=gcc
# NOTE > clang feels like a hipster compiler.
#CC=clang

#=======================================================================
# Directories                                                          =
#=======================================================================
ROOT=/
INC=./include
SRC=./src
BIN=./bin

#=======================================================================
# FLAGS                                                                =
#=======================================================================
# Include flag
IFLAG=-Iinclude -I../customConfigs/include

# Library flag
LFLAG=-lSDL2 -L../customConfigs/lib -llpc -Wl,-rpath=../customConfigs/lib

# Output flag
OFLAG=-o $(BIN)/side

# Bin file list
BIN_FILES=
#	$(BIN)/sdl-manager.o \
#	$(BIN)/sdl-event-manager.o \
#	$(BIN)/updateTick.o \
#	$(BIN)/drawTick.o \
#	$(BIN)/finalizeTick.o

#=======================================================================
# NOTE > I call out each file in the system, knowing it is not as      =
#      > maintainable                                                  =
#      > as it could be. The reason is to always be aware of the       =
#      > makefile and                                                  =
#      > to make sure it is always as clean as it can be.              =
#=======================================================================
all:clean mkdirs $(BIN_FILES)
	$(CC) $(IFLAG) $(BIN_FILES) $(SRC)/main.c $(OFLAG) $(LFLAG)
	$(BIN)/side

$(BIN)/sdl-manager.o:
	$(CC) $(IFLAG) $(SRC)/sdl-manager.c -c -o $(BIN)/sdl-manager.o

$(BIN)/sdl-event-manager.o:
	$(CC) $(IFLAG) $(SRC)/sdl-event-manager.c -c -o $(BIN)/sdl-event-manager.o

$(BIN)/updateTick.o:
	$(CC) $(IFLAG) $(SRC)/updateTick.c -c -o $(BIN)/updateTick.o

$(BIN)/drawTick.o:
	$(CC) $(IFLAG) $(SRC)/drawTick.c -c -o $(BIN)/drawTick.o

$(BIN)/finalizeTick.o:
	$(CC) $(IFLAG) $(SRC)/finalizeTick.c -c -o $(BIN)/finalizeTick.o

clean:
	rm -rf $(BIN)/*

mkdirs:clean
	mkdir -p $(BIN)

copy-required:
	cp $(ROOT)/home/tbalz_000/TOOLS/SDL2-2.0.0/bin/SDL2.dll $(BIN)
